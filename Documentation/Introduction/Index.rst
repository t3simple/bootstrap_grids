.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt
.. include:: Images.txt


Introduction
============


What does it do?
----------------

Provides an import package with predefined gridelements and the corresponding typoscript setup.


Screenshot
----------

|img-screenshot|

Grid elements included in this extension.
