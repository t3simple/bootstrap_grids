..  Editor configuration
...................................................
* utf-8 with BOM as encoding
* tab indent with 4 characters for code snippet.
* optional: soft carriage return preferred.

.. Includes roles, substitutions, ...
.. include:: Includes.txt



=====================
Grids for bootstrap
=====================

:Extension key: bootstrap_grids
:Author: Pascal Mayer
:Email: typo3@simple.ch
:Language: en


This document is published under the Open Content License

available from http://www.opencontent.org/opl.shtml

The content of this document is related to TYPO3

\- a GNU/GPL CMS/Framework available from www.typo3.org


**Table of Contents**

.. toctree::
   :maxdepth: 5

   Introduction/Index
   Administration/Index
   Configuration/Index
